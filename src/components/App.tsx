import React, { ChangeEvent, FC, FormEvent, useEffect, useRef, useState } from 'react';
import './App.scss';
import ImageElement from './image-element/image-element';
import { Images, PropsImage } from '../utils/types';
import { reqApi } from '../utils/utils';
import { TAGS } from '../utils/constansts';


const App: FC = () => {
  const [allImages, setAllIMages] = useState<any>({});
  const [images, setImages] = useState<Images[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [flagButton, setFlagButton] = useState<boolean>(false);
  const [value, setValue] = useState<string>('');
  const [valueFocus, setValueFocus] = useState<boolean>(false);
  const [valueError, setValueError] = useState<string>("заполните поле 'тег'");
  const [errorMessage, setErrorMessage] = useState<string>('');
  const [groupImage, setGroupImage] = useState<JSX.Element[]>([]);
  const [isDelay, setIsDelay] = useState<boolean>(false);
  const count = useRef<number>(0);

  const submitHandler = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setIsDelay(false);
    setErrorMessage('');
    if (value.length === 0) {
      setValueFocus(true);
    } else {
      setLoading(true);
      setImages([]);
      if (value === 'delay') {
        setIsDelay(true);
      } else {
        const reg = /[a-zA-Z]+/g;
        const arrReg = value.match(reg);
        if (arrReg === null) {
          return console.error('error in submitHandler');
        }
        count.current = arrReg.length;
        arrReg.forEach((tag) => {
          reqApi(
            tag,
            setErrorMessage,
            setLoading,
            setImages,
            setAllIMages,
            setValueFocus,
            allImages,
          );
        });
      }
    }
  };

  const addInput = (e: ChangeEvent<HTMLInputElement>) => {
    setErrorMessage('');
    const value = e.target.value;
    const reg = /^([a-z]*,?)*$/;
    if (reg.test(String(value).toLowerCase())) {
      setValue(value);
      setValueError('');
    }
    if (value.length === 0) {
      setValueError("заполните поле 'тег'");
    }
  };

  const clearHandler = () => {
    setImages([]);
    setFlagButton(false);
    setValue('');
    setGroupImage([]);
    setAllIMages({});
    setLoading(false);
    setErrorMessage('');
    setValueFocus(false);
    setIsDelay(false);
  };

  const imageArr = (
    <ul className="ungroup-list">
      {groupImage.map((data, index) => {
        return (
          <li className="card" key={index}>
            {data}
          </li>
        );
      })}
    </ul>
  );

  const arrMapping = (array: Images[]) => {
    return array.map((data, index) => {
      return <ImageElement setValue={setValue} dataImage={data} key={index} />;
    });
  };

  const imageCard = <ul className="card ungroup-list__item">{arrMapping(images)}</ul>;

  const GroupElement = ({ objImage, name }: PropsImage) => {
    return (
      <div className="card group-list">
        <p className="group-list__title">{name}</p>
        <ul className="group-list__container">{arrMapping(objImage)}</ul>
      </div>
    );
  };

  const allGroupElements = Object.keys(allImages).map((value, index) => {
    return <GroupElement key={index} objImage={allImages[value]} name={value} />;
  });

  useEffect(() => {
    if (images.length && count.current === images.length) {
      setValue('');
      setLoading(false);
      setValueFocus(false);
      setValueError("заполните поле 'тег'");
      setGroupImage((data) => [...data, imageCard]);
    }
  }, [images.length]);

  useEffect(() => {
    if (isDelay) {
      const timer = setInterval(() => {
        count.current = 1;
        setImages([]);
        const tag = TAGS[Math.floor(Math.random() * TAGS.length - 1)];
        reqApi(tag, setErrorMessage, setLoading, setImages, setAllIMages, setValueFocus, allImages);
      }, 5000);
      return () => clearInterval(timer);
    }
  }, [allImages, isDelay]);

  return (
    <div className="App container">
      <form onSubmit={submitHandler} className="form-inline form">
        <div className=" form__input mx-sm-2">
          <input
            placeholder="Введите тег"
            onChange={addInput}
            value={value}
            type="text"
            className="form-control"
          />
          <div className="form__error">
            {errorMessage}
            {valueFocus && valueError && valueError}
          </div>
        </div>
        <button type="submit" className="btn btn-success mx-sm-2 " disabled={loading}>
          {loading ? 'Загузка' : 'Загрузить'}
        </button>
        <button onClick={clearHandler} type="button" className="btn btn-danger mx-sm-2 ">
          Очистить
        </button>
        {flagButton ? (
          <button
            onClick={() => setFlagButton(false)}
            type="button"
            className="btn btn-info mx-sm-2">
            Разгруппировать
          </button>
        ) : (
          <button
            onClick={() => setFlagButton(true)}
            type="button"
            className="btn btn-info mx-sm-2">
            Группировать
          </button>
        )}
      </form>
      <main className="main">{flagButton ? allGroupElements : imageArr}</main>
    </div>
  );
};

export default App;
