import React from 'react';
import {DataImage} from '../../utils/types';
import './image-element.scss';

const ImageElement = ({dataImage, setValue}: DataImage) => {
  const imageHandler = (e: React.MouseEvent<HTMLImageElement>) => {
    const dataSet = e.currentTarget.dataset.name;
    if(dataSet === undefined) {
      return undefined;
    }
    setValue(dataSet);
  };
  return (
    <li>
      <img onClick={imageHandler} data-name={dataImage.tag} className="image-element" src={dataImage.link} alt="смешное изображение"/>
    </li>
  )

}

export default ImageElement;