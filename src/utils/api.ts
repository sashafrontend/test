class Api  {
  _baseUrl = 'https://api.giphy.com/v1/gifs/';
  _apiKey = 'JgJ1QiQrCrwlH0SDYOpM6Ph7MkUjDH3x';

  getImage = async (tag: string) => {
    try {
      const response =  await fetch(`${this._baseUrl}random?api_key=${this._apiKey}&tag=${tag}`)
      if(!response.ok) {
        return Promise.reject(`error ${response.status}`);
      }
      const data = await response.json();
      return data.data;
    }
    catch (err) {
      console.error(err);
    }
  }

};

export const api = new Api();
