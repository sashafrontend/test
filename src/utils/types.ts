export interface Images {
  link: string;
  tag: string;
}

export interface PropsImage {
  objImage: Images[];
  name: string;
}

export interface DataImage {
  dataImage: Images;
  setValue: any;
}