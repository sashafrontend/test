import { Images } from './types';
import { api } from './api';

export const reqApi = (
  tag: string,
  setErrorMessage: (value: ((prevState: string) => string) | string) => void,
  setLoading: (value: ((prevState: boolean) => boolean) | boolean) => void,
  setImages: (value: ((prevState: Images[]) => Images[]) | Images[]) => void,
  setAllIMages: any,
  setValueFocus: (value: ((prevState: boolean) => boolean) | boolean) => void,
  allImages: any,
) => {
  api.getImage(tag).then((data) => {
    const link = data.image_url;
    if (data.length === 0) {
      setErrorMessage('По тегу ничего не найдено');
      setLoading(false);
    } else {
      setImages((data: any) => [...data, { link, tag }]);
      if (tag in allImages) {
        setAllIMages((obj: any) => ({
          ...obj,
          [tag]: [...obj[tag], { link, tag }],
        }));
      } else {
        setAllIMages((obj: any) => ({ ...obj, [tag]: [{ link, tag }] }));
      }
    }
  })
  .catch(() => {
    setErrorMessage('Произошла http ошибка');
    setValueFocus(false);
    setLoading(false);
  });
};